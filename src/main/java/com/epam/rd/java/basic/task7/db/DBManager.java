package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String FULL_URL = "jdbc:derby:memory:testdb;create=true";

    private static final String COL_ID = "id";
    private static final String COL_LOGIN = "login";
    private static final String COL_NAME = "name";

    private static final String FIND_ALL_USERS = "SELECT * FROM users";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String GET_USER = "SELECT * FROM users WHERE login = ?";
    private static final String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
    private static final String GET_USER_TEAMS = "SELECT t.* FROM users_teams ut\n" +
            "JOIN teams t ON ut.team_id = t.id\n" +
            "WHERE ut.user_id = ?";
    private static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
        try (Connection con = DriverManager.getConnection(FULL_URL)) {
            System.out.println("Connected " + con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_USERS)) {
            while (rs.next()) {
                User user = User.createUser(rs.getString(COL_LOGIN));
                user.setId(rs.getInt(COL_ID));
                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: findAllUsers was failed.", e);
        }
        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        boolean result;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            prst.setString(1, user.getLogin());
            result = prst.execute();
            try (ResultSet rs = prst.getGeneratedKeys()) {
                if (rs.next())
                    user.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: insertUser was failed.", e);
        }
        return result;
    }

    public boolean deleteUsers(User... users) throws DBException {
        boolean result = false;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(DELETE_USER)) {
            for (User user : users) {
                prst.setInt(1, user.getId());
                result = prst.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: deleteUsers was failed.", e);
        }
        return result;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(GET_USER)) {
            prst.setString(1, login);
            try (ResultSet rs = prst.executeQuery()) {
                if (rs.next()) {
                    user = User.createUser(rs.getString(COL_LOGIN));
                    user.setId(rs.getInt(COL_ID));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: getUser was failed.", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(GET_TEAM)) {
            prst.setString(1, name);
            try (ResultSet rs = prst.executeQuery()) {
                if (rs.next()) {
                    team = Team.createTeam(rs.getString(COL_NAME));
                    team.setId(rs.getInt(COL_ID));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: getUser was failed.", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(FIND_ALL_TEAMS)) {
            while (rs.next()) {
                Team team = Team.createTeam(rs.getString(COL_NAME));
                team.setId(rs.getInt(COL_ID));
                teamList.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: findAllTeams was failed.", e);
        }
        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        boolean result;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            prst.setString(1, team.getName());
            result = prst.execute();
            try (ResultSet rs = prst.getGeneratedKeys();) {
                if (rs.next())
                    team.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: insertTeam was failed.", e);
        }
        return result;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement prst = null;
        boolean result = false;
        try {
            con = DriverManager.getConnection(FULL_URL);
            con.setAutoCommit(false);
            prst = con.prepareStatement(SET_TEAMS_FOR_USER);
            for (Team team : teams) {
                prst.setInt(1, user.getId());
                prst.setInt(2, team.getId());
                result = prst.execute();
            }
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            rollback(con);
            throw new DBException("DBManager: setTeamsForUser was failed.", e);
        } finally {
            close(prst);
            close(con);
        }
        return result;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(GET_USER_TEAMS)) {
            prst.setInt(1, user.getId());
            try (ResultSet rs = prst.executeQuery()) {
                while (rs.next()) {
                    Team team = Team.createTeam(rs.getString(COL_NAME));
                    team.setId(rs.getInt(COL_ID));
                    teamList.add(team);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: getUserTeams was failed.", e);
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean result = false;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(DELETE_TEAM)) {
            prst.setInt(1, team.getId());
            result = prst.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: deleteTeam was failed.", e);
        }
        return result;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean result = false;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement prst = con.prepareStatement(UPDATE_TEAM)) {
            prst.setString(1, team.getName());
            prst.setInt(2, team.getId());
            result = prst.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("DBManager: updateTeam was failed.", e);
        }
        return result;
    }

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
